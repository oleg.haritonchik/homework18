#include <iostream>

using namespace std;

template<typename T>
class Stack
{
public:

	void FillArray(T* const arr, const int size)
	{
		for (int i = 0; i < size; i++)
		{
			arr[i] = char('a' + rand() % 26);
		}
	}

	void ShowArray(T* const arr, const int size)
	{
		for (int i = 0; i < size; i++)
		{
			cout << arr[i] << "\t";
		}
	}

	void Fill_Print(T* arr, const int size)
	{
		//Fill&Print Array
		FillArray(arr, size);
		cout << "Array" << "\t\t";
		ShowArray(arr, size);
		cout << "\n=======================================================\n";
	}

	void SwapArray(T* Array1, T* Array2, const int size)
	{
		cout << "Enter 1) Swap Array Elements 1->2 2) Swap Array Elements 2->1:\t";
		cin >> swap;

		while (swap != 1 || 2)
		{

			switch (swap)
			{
			case 1:

				delete[]Array2;
				Array2 = new T[size];

				for (int i = 0; i < size; i++)
				{
					Array2[i] = Array1[i];
				}

				cout << "\n\n\After Swap\n\n";
				cout << "\n=======================================================\n";
				cout << "Array1= \t";

				ShowArray(Array1, size);
				cout << "\n=======================================================\n";
				cout << "Array2= \t";

				ShowArray(Array2, size);
				cout << "\n=======================================================\n";

				delete[]Array2;
				delete[]Array1;

				break;
			case 2:

				delete[]Array1;
				Array1 = new T[size];

				for (int i = 0; i < size; i++)
				{
					Array1[i] = Array2[i];
				}
				cout << "\n\n\After Swap\n\n";
				cout << "\n=======================================================\n";
				cout << "Array1= \t";

				ShowArray(Array1, size);
				cout << "\n=======================================================\n";
				cout << "Array2= \t";

				ShowArray(Array2, size);
				cout << "\n=======================================================\n";

				delete[]Array1;
				delete[]Array2;

				break;
			default:

				cout << "Enter 1 or 2\n";
				break;
			}
			cin >> swap;
		}
	}

	void push_back(T*& arr, int& NewSize, T& NewValue)
	{
		T* NewArray = new T[NewSize + 1];
		for (int i = 0; i < NewSize; i++)
		{
			NewArray[i] = arr[i];
		}
		NewArray[NewSize] = NewValue;
		NewSize++;
		delete arr;
		arr = nullptr;
		arr = NewArray;
		cout << "\n\nNew Array + 1 Element\t";
		cout << "\n=======================================================\n";
		cout << "New Array\t";
		ShowArray(arr, NewSize);
		cout << "\n=======================================================\n";
	}

	void pop_back(T*& arr, int& NewSize)
	{
		T* NewArray = new T[NewSize - 1];
		NewSize--;
		for (int i = 0; i < NewSize; i++)
		{
			NewArray[i] = arr[i];
		}
		delete arr;
		arr = nullptr;
		arr = NewArray;
		cout << "\n\nNew Array - 1 Element\t";
		cout << "\n=======================================================\n";
		cout << "New Array\t";
		ShowArray(arr, NewSize);
		cout << "\n=======================================================\n";

	}
private:
	int size;
	int swap;


};


int main()
{

	//Enter Data
	int size = 5;
	cout << "Enter Size: \t";
	cin >> size;

	//Initialize Array

	char* Array1 = new char[size];
	char* Array2 = new char[size];

	cout << "\n=======================================================\n";

	//Print Arrays
	Stack<char> ver1;
	ver1.Fill_Print(Array1, size);
	ver1.Fill_Print(Array2, size);

	char NewValue;
	cout << "\n\Enter Value For New Element\n\n";
	cin >> NewValue;
	//NewValue = rand() % 100;


	//Add, Delete Element
	ver1.push_back(Array1, size, NewValue);
	ver1.pop_back(Array1, size);
	ver1.pop_back(Array2, size);
	ver1.push_back(Array2, size, NewValue);


	//Swap Arrray Elements 
	cout << "\n\Swap Elements\n\n";
	ver1.SwapArray(Array1, Array2, size);

	delete[]Array1;
	delete[]Array2;
}




